<?php
define("APP_PATH", dirname(dirname(__FILE__)));

//load databade
include APP_PATH."/.env/config.php";
//load controller
include APP_PATH."/bank_controller/controllers.php";
//load Router
include APP_PATH."/bank_routes/routes.php";

?>
