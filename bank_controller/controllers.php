<?php 
define("DB_PATH", dirname(dirname(__FILE__)));
include DB_PATH."/bank_model/entity_db.php";
include DB_PATH."/bank_model/interface_db.php";

 function doesUserNameExist($dbconn, $input){
 	$result = false;
 	$check = $dbconn->prepare("SELECT * FROM admin WHERE username=:usr");
 	$check->bindParam(":usr", $input);
 	$check->execute();

 	if ($check->rowCount() > 0) {
 		$result = true;
 	}
 	return $result;
 }


 function registerCustomer($dbconn, $input){
 	$check = $dbconn->prepare("SELECT * FROM account_type WHERE account_type_name=:id");
 	$check->bindParam(':id', $_POST['act']);
 	$check->execute();
 	$row = $check->fetch(PDO::FETCH_BOTH);
 	var_dump($row);
 	if($check->rowCount() > 0 && $_POST['balance'] > $row['maximum_opening_balance'] ){
 		header("Location:c_register?error=Too Much Funds");
 		die();
 	}
 	if($check->rowCount() > 0 && $_POST['balance'] < $row['minimum_opening_balance'] ){
 		header("Location:c_register?error=Insufficient Funds");
 		die();
 	}
 	$gen_num = rand(1000000,9999999);
 	$account_number = "309".$gen_num;
 	$hash = rand(1000,9999);

 	$sendData = $dbconn->prepare("INSERT INTO customer VALUES(NULL,:nm,:act,:anu,:bal,:hs,NOW(),NOW() )");
 	$sendData->bindParam(':nm', $input['name']);
 	$sendData->bindParam(':act', $input['act']);
 	$sendData->bindParam(':anu', $account_number);
 	$sendData->bindParam(':bal', $_POST['balance']);
 	$sendData->bindParam('hs', $hash);
 	$sendData->execute();
 }

 function UpdateBal($dbconn,$id, $input){
 	var_dump($input);
 	var_dump($id);
 	$recipientQuery = $dbconn->prepare("SELECT * FROM customer WHERE customer_id =:id");
 	$recipientQuery->bindParam(':id', $id);
 	$recipientQuery->execute();

$recipientInfo = $recipientQuery->fetch(PDO::FETCH_BOTH);
$recipient = $recipientInfo['account_type_name'];
//var_dump($recipient);

$recipientPreviousBalance = $recipientInfo['account_balance'];
$senderFinalBalance = $recipientPreviousBalance + $_POST['bal'];

$check = $dbconn->prepare("SELECT * FROM account_type WHERE account_type_name=:id");
$check->bindParam(':id',$recipient);
$check->execute();
$row = $check->fetch(PDO::FETCH_BOTH);
//var_dump($row);
if($check->rowCount() > 0 && $senderFinalBalance > $row['maximum_balance'] ){
	header("Location:fund?error=Too Much Funds");
	die();
 	}



 	$Update = $dbconn->prepare("UPDATE customer set account_balance =:bal WHERE customer_id=:cid");
 	$Update->bindParam(':bal', $senderFinalBalance);
 	$Update->bindParam(':cid', $id);
 	$Update->execute();
 }

 function userData($dbconn){

$stmt = $dbconn->prepare("SELECT * FROM customer");
$stmt->execute();
while($row = $stmt->fetch(PDO::FETCH_BOTH)){
extract($row);
$result[] = $row;
 }
 return $result;
}

function userAccountName($dbconn){

$stmt = $dbconn->prepare("SELECT account_name FROM customer");
$stmt->execute();
while($row = $stmt->fetch(PDO::FETCH_BOTH)){
extract($row);
$result[] = $row;
 }
 return $result;
}





function registerAdmin($dbconn, $input){
	$encrypted = password_hash($_POST['pw'],PASSWORD_BCRYPT); 
	$sendAdminData = $dbconn->prepare("INSERT INTO admin VALUES (NULL,:fn,:usr,:hs,NOW(),NOW() )");
	$sendAdminData->bindParam(":fn", $_POST['name']);
	$sendAdminData->bindParam(":usr", $_POST['usr']);
	$sendAdminData->bindParam(":hs", $encrypted);
	$sendAdminData->execute();
	header("Location:/login");
}

function getAccountType($dbconn){
$stmt = $dbconn->prepare("SELECT * FROM account_type");
$stmt->execute();
while ($row = $stmt->fetch(PDO::FETCH_BOTH)) {
	extract($row);
	var_dump($row);
    echo '<option value="'.$account_type_name.'">'.$account_type_name.'</option>';

}
}

function getAccountTypeDetails($dbconn){
$stmt = $dbconn->prepare("SELECT * FROM account_type");
$stmt->execute();
while($row = $stmt->fetch(PDO::FETCH_BOTH)){
extract($row);
$result[] = $row;
 }
 return $result;
}

function loginAdmin($dbconn, $input){
	$checkLogin = $dbconn->prepare("SELECT * FROM admin WHERE username=:usr");
	$checkLogin->bindParam(":usr",$_POST['login_name']);
	$checkLogin->execute();
	$row = $checkLogin->fetch(PDO::FETCH_BOTH);
	var_dump($row);
if($checkLogin->rowCount() > 0 && password_verify($_POST['login_hash'] ,$row['hash']) ){
		$_SESSION['id'] = $row[0];
		$_SESSION['name'] = $row[1];
		header("Location:dashboard");
	}
}

function sendMessage($dbconn,$id,$input){
	$getSenderDetails = $dbconn->prepare("SELECT * FROM customer WHERE customer_id =:cid");
	$getSenderDetails->bindParam(':cid', $_SESSION['id']);
	$getSenderDetails->execute();
	$senderInfo = $getSenderDetails->fetch(PDO::FETCH_BOTH);

	$recieverDetails = $dbconn->prepare("SELECT * FROM customer WHERE customer_id=:id");
	$recieverDetails->bindParam(":id", $id);
	$recieverDetails->execute();
	$recipientInfo = $recieverDetails->fetch(PDO::FETCH_BOTH);


	$senderCustomerId = $senderInfo['customer_id'];
	$senderMessageType = "SENT";
	$senderMessages = $input['text'];

	$sender = $dbconn->prepare("INSERT INTO messages VALUES(NULL,:cid,:mt,:msg,NOW(),NOW())");
	$sender->bindParam(':cid', $senderCustomerId);
	$sender->bindParam(':mt', $senderMessageType);
	$sender->bindParam(':msg', $senderMessages);
	$sender->execute();



$recipientCustomerId = $recipientInfo['customer_id'];
$recipientMessageType = "RECEIVE";
$recipientMessages = $input['text'];
$recipient = $dbconn->prepare("INSERT INTO messages VALUES(NULL,:cid,:mt,:msg,NOW(),NOW())");
$data = [
	":cid" => $recipientCustomerId,
	":mt"  => $recipientMessageType,
	":msg" => $recipientMessages, 
];
	$recipient->execute($data);
}

function messages($dbconn, $id){
	$messages = $dbconn->prepare("SELECT DISTINCT messages FROM messages WHERE customer_id=:cid AND :id");
	$messages->bindParam(':cid', $_SESSION['id']);
	$messages->bindParam(':id', $id);
	$messages->execute();
	while($allMessages = $messages->fetch(PDO::FETCH_BOTH)){
		var_dump($allMessages);
	}
} 

function registerType($dbconn, $input){
	$registerType = $dbconn->prepare("INSERT INTO account_type VALUES (NULL,:at,:mobi,:xobi,:mbi,:xbi,NOW(),NOW() )");
	$registerType->bindParam(':at', $_POST['act']);
	$registerType->bindParam(':mobi', $_POST['mob']);
	$registerType->bindParam(':xobi', $_POST['xob']);
	$registerType->bindParam(':mbi', $_POST['mb']);
	$registerType->bindParam(':xbi', $_POST['xb']);
	$registerType->execute();
}

// function regType($dbconn, $input){
// 	$regType = $dbconn->prepare("INSERT INTO type(id, account, mini_bal, maxi_bal,min_bal,max_bal, time_created,date_created) VALUES(NULL,:at,:mobi,:xobi,:mb,:xb,NOW(),NOW() )");
// 	$data = [
// 		'at' => $input['act'],
// 		'mobi' => $input['mob'],
// 		'xobi' => $input['xob'],
// 		'mbi' => $input['mb'],
// 		'xbi' => $input['xb'],
// 	];
// 	$regType->execute($data);
// }

function delete($dbconn, $id){
	$remove = $dbconn->prepare("DELETE FROM customer WHERE customer_id=:id");
	$remove->bindParam(':id', $id);
	$remove->execute();
	var_dump($id);

}

function transferFund($dbconn,$id,$input){
	$senderQuery = $dbconn->prepare("SELECT * FROM customer WHERE customer_id=:id");
	$senderQuery->bindParam(':id', $_SESSION['id']);
	$senderQuery->execute();
	$senderInfo = $senderQuery->fetch(PDO::FETCH_BOTH);

	if ($_POST['amount'] > $senderInfo['account_balance']) {
		header("Location:/transfer");
		die();
	}

	if ($_POST['accNum'] == $senderInfo['account_number']) {
		header("Location:/transfer");
		die();
	}
	$recipientQuery = $dbconn->prepare("SELECT * FROM customer WHERE account_number=:accNum");
	$recipientQuery->bindParam(":accNum", $_POST['accNum']);
	$recipientQuery->execute();
	if ($recipientQuery->rowCount() < 1) {
	header("Location:/transfer");
	die();
	}
	$recipientInfo = $recipientQuery->fetch(PDO::FETCH_BOTH);

	$senderPreviousBalance = $senderInfo['account_balance'];
	$receiverPreviousBalance = $recipientInfo['account_balance'];

	$checkSenderAccountType = $dbconn->prepare("SELECT * FROM account_type WHERE account_type_name = :atn");
	$checkSenderAccountType->bindParam(":atn", $senderInfo['account_type_name']);
	$checkSenderAccountType->execute();
	$checkSenderAccountTypeInfo = $checkSenderAccountType->fetch(PDO::FETCH_BOTH);
	// var_dump($checkSenderAccountTypeInfo);

	$senderFinalBalance = $senderPreviousBalance - $_POST['amount'];
	if ($senderFinalBalance < $checkSenderAccountTypeInfo['minimum_balance']) {
	header("Location:/transfer");
	die();
	}

	$updateSenderAccount = $dbconn->prepare("UPDATE customer SET account_balance=:sac WHERE account_number=:san");
	$updateSenderAccount->bindParam(':sac', $senderFinalBalance);
	$updateSenderAccount->bindParam(':san', $senderInfo['account_number']);
	$updateSenderAccount->execute();


$senderLogCustomerId = $senderInfo['customer_id'];
$senderLogTransactionType = "DEBIT";
$senderLogSenderAccount = $senderInfo['account_number'];
$senderLogReceiverAccount = $recipientInfo['account_number'];
$senderLogPreviousBalance = $senderInfo['account_balance'];
$senderLogTransactionAmount = $_POST['amount'];
$senderLogFinalBalance = $senderFinalBalance;

$senderLog = $dbconn->prepare("INSERT INTO transaction VALUES(NULL,:cid,:stt,:ssa,:sra,:spb,:sta,:sfb,NOW(),NOW() ) ");
$data = [
	':cid' => $senderLogCustomerId,
	":stt" => $senderLogTransactionType,
	":ssa" => $senderLogSenderAccount,
	":sra" => $senderLogReceiverAccount,
	":spb" => $senderLogPreviousBalance,
	":sta" => $senderLogTransactionAmount,
	":sfb" => $senderLogFinalBalance
];
$senderLog->execute($data);

$checkRecieverAccountType = $dbconn->prepare("SELECT * FROM account_type WHERE account_type_name = :atn");
$checkRecieverAccountType->bindParam(":atn", $recipientInfo['account_type_name']);
$checkRecieverAccountType->execute();
$checkRecieverAccountTypeInfo = $checkRecieverAccountType->fetch(PDO::FETCH_BOTH);
var_dump($checkRecieverAccountTypeInfo);


$recipientFinalBalance = $receiverPreviousBalance + $_POST['amount'];
if ($recipientFinalBalance > $checkRecieverAccountTypeInfo['maximum_balance']) {
	header("Location:/transfer");
	die();
	}

$updateRecieverAccount = $dbconn->prepare("UPDATE customer SET account_balance=:sac WHERE account_number=:san");
$updateRecieverAccount->bindParam(':sac', $recipientFinalBalance);
$updateRecieverAccount->bindParam(':san', $recipientInfo['account_number']);
$updateRecieverAccount->execute();
///////////////////////////////////////////////////////////////////
$recieverLogCustomerId = $recipientInfo['customer_id'];
$recieverLogTransactionType = "CREDIT";
$recieverLogSenderAccount = $senderInfo['account_number'];
$recieverLogReceiverAccount = $recipientInfo['account_number'];
$recieverLogPreviousBalance = $recipientInfo['account_balance'];
$recieverLogTransactionAmount = $_POST['amount'];
$recieverLogFinalBalance = $recipientFinalBalance;

$recieverLog = $dbconn->prepare("INSERT INTO transaction VALUES(NULL,:cid,:rtt,:rsa,:rra,:rpb,:rta,:rfb,NOW(),NOW() ) ");
$data = [
	':cid' => $recieverLogCustomerId,
	":rtt" => $recieverLogTransactionType,
	":rsa" => $recieverLogSenderAccount,
	":rra" => $recieverLogReceiverAccount,
	":rpb" => $recieverLogPreviousBalance,
	":rta" => $recieverLogTransactionAmount,
	":rfb" => $recieverLogFinalBalance
];
$recieverLog->execute($data);

}



 ?>

