<?php
session_start();
if (array_key_exists('submit', $_POST)) {
	$error = [];
	if (empty($_POST['text'])) {
		$error[] = "Enter message";
	}
	if (empty($error)) {
		$clean = array_map('trim', $_POST);
		sendMessage($econn, $id, $clean);
	}
}

?>


<!DOCTYPE html>
<html>
<head><meta charset="utf-8">
		<title>Swipe – The Simplest Chat Platform</title>
</head>
<body>

	<h1>Chat Bot</h1>
	<p>Send Messages Seamlessly...</p>
	<?php messages($econn, $id); ?>


	<form class="position-relative w-100" method="POST">
		<input type="text" name="text">
		<input type="submit" name="submit" value="SEND">
	</form>

</body>
</html>




























</body>
</html>