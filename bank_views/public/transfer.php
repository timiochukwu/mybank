<?php 
session_start();

if (array_key_exists('submit', $_POST)) {
	$error = [];

	if (empty($_POST['accNum'])) {
		$error = "Enter an account Number";
	}

	if (!is_numeric($_POST['accNum'])) {
		$error = "Enter a valid account number";
	}
	if (empty($_POST['amount'])) {
		$error = "Enter an amount";
	}

	if (!is_numeric($_POST['amount'])) {
		$error = "Enter a valid amount";
	}
	if (empty($error)) {
		$clean = array_map('trim', $_POST);
		transferFund($econn,$id, $clean);
}
}
 ?>


<!DOCTYPE html>
<html>
<head>
	<title>Transfer</title>
</head>
<body>
	<form method="POST">
		<p>
			<label>Account Number</label><br>
			<input type="text" name="accNum" placeholder="Account Number">
		</p>
		<p>
			<label>Amount</label><br>
			<input type="text" name="amount" placeholder="Amount">
		</p>
		<input type="submit" name="submit" value="submit">
		
	</form>

</body>
</html>