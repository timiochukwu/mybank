<?php 

if (array_key_exists('submit', $_POST)) {
	$error = [];
	if (empty($_POST['name'])) {
		$error[] = "Enter Your Name";
	}

	if (empty($_POST['usr'])) {
		$error[] ="Enter Your Username";
	}

	if (doesUserNameExist($econn,$_POST['usr'])) {
		$error['usr'] = "Username already exist";
	}

	if (empty($_POST['pw'])) {
		$error[] ="Enter Your Password";
	}

	if (empty($_POST['cpw'])) {
		$error[] ="Confirm Your Password";
	}
	if(empty($error)) {
		$clean = array_map('trim', $_POST);
		registerAdmin($econn, $clean);
	}
}



 ?>


<!DOCTYPE html>
<html>
<head>
	<title>Admin Registration</title>
</head>
<body>

	<h1>Admin Registration Panel</h1>

	<form action="" method="post">
		<label>Full Name</label><br>
		<input type="text" name="name"><br>

		<label>Username</label><br>
		<input type="text" name="usr"><br>


		<label>Password</label><br>
		<input type="password" name="pw"><br>
		
		<label>Confirm Password</label><br>
		<input type="password" name="cpw"><br>
		<input type="submit" name="submit" value="Submit">
		
	</form>

</body>
</html>