<?php
session_start();  
$getAccountTypeDetails = getAccountTypeDetails($econn);
if (array_key_exists("go", $_POST)) {
	$error = [];

	if (empty($_POST['act'])) {
		$error[] = "Kindly Enter Select Account Type";
	}

	if (empty($_POST['mob'])) {
		$error[] = "Kindly Enter Account Minimum Opening Balance";
	}

	if (empty($_POST['xob'])) {
		$error[] = "Kindly Enter Account Maximum Opening Balance";
	}

	if (empty($_POST['mb'])) {
		$error[] = "Kindly Enter Account Minimum Balance";
	}

	if (empty($_POST['xb'])) {
		$error[] = "Kindly Enter Account Maximum Balance";
	}
	if (empty($error)) {
		$clean = array_map('trim', $_POST);
		registerType($econn, $clean);
	
	}
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Create Type </title>
</head>
<body>

<?php include 'header/header.php'; ?>
<p>This page allows you to create various account type, set balance limit and can adjust limits</p>

<table border="1">
	<tr>
		<th>Account Type Name</th>
		<th>Minimum Opening Balance</th>
		<th>Maximum Opening Balance</th>
		<th>Minimum Balance</th>
		<th>Maximum Balance</th>
	</tr>
	<?php foreach ($getAccountTypeDetails as $key => $value): ?>
		<tr>
			<td><?= $value['account_type_name'] ?></td>
			<td><?= $value['minimum_opening_balance'] ?></td>
			<td><?= $value['maximum_opening_balance'] ?></td>
			<td><?= $value['minimum_balance'] ?></td>
			<td><?= $value['maximum_balance'] ?></td>
		</tr>
	<?php endforeach ?>

</table>
<hr>
<form action="" method="post">
	<label>Account Type</label>
	<input type="text" name="act" placeholder="--Select Account Type--">
	<br><br>

	<label>Minimum Opening Balance</label>
	<input type="text" name="mob"><br><br>

	<label>Maximum Opening Balance</label>
	<input type="text" name="xob"><br><br>

	<label>Minimum Balance</label>
	<input type="text" name="mb"><br><br>

	<label>Maximum Balance</label>
	<input type="text" name="xb"><br><br>
	<input type="submit" name="go" value="SUBMIT">

</form>



</body>
</html>



