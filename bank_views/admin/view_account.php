<?php

session_start();
$userData = userData($econn)
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View Accounts</title>
</head>

<body>
	
<?php include 'header/header.php'; ?>

<!-- Creating a table for Accounts fectched from database -->
<table border="1">
<tr>
	<th>Customer Id</th>
    <th>Account Name</th>
    <th>Account Number</th>
    <th>Account Balance</th>
    <th>Accont Type</th>
    <th>Date Registered</th>
    <th>Time Registered</th>
    <th>Option</th>
    <th>Big Option</th>
</tr>
<?php foreach ($userData as $key => $value): ?>
	<tr>
<td> <?= $value['customer_id'] ?></td>
<td> <?= $value['account_name']?> </td>
<td> <?= $value['account_number'] ?></td>
<td> <?= $value['account_type_name'] ?></td>
<td> <?= $value['account_balance'] ?></td>
<td> <?= $value['date_created'] ?></td>
<td> <?php echo $value['time_created'] ?></td>
<td><a href="/fund?id=<?php echo $value['customer_id'] ?>">Edit</a> || <a href="/delete?id=<?= $value['customer_id'] ?>">DELETE</a></td>
  <td><a href="/act?id=<?php echo $value['customer_id']; ?>">Activate</a> || <a href="/suspend?id=<?php echo $value['customer_id']; ?>">Suspend</a></td>
</tr>
	
<?php endforeach ?>


</table>


</body>
</html>